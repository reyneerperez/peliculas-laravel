<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

const API_KEY = '669197cc96c9a18684708d9391641de3';
const URL = 'https://api.themoviedb.org/3/movie/';
class MovieController extends Controller
{
    public function popular(Request $request){
        $client = new Client();
        $response = $client->request('GET', URL.'popular', [
            'query' => [
                'api_key' => API_KEY,
                'language' => 'en-US',
                'page' => $request->page
            ]
        ]);
        return $response;
    }
}
